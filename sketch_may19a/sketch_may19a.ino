#include <dht.h>


dht my_dht_1;
 
#define dht_pin_7 7
#define solo_pin_A0 A0
float percentual_umidade ;
int delay1 = 20000;
int delay2 = 30000;

/************************************************************
  Sensor Temperatura/Umidade 1 - Estufa Controlada
 ************************************************************/
float temperatura_7 = 0x00;
float umidade_7     = 0x00;

/************************************************************
  Sensor Solo 1 - Estufa Controlada
 ************************************************************/
float solo_A0 ;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
 
 }

void loop() {
 /************************************************************
   Estufa 1 - Estufa Controlada
 ************************************************************/
 my_dht_1.read11(dht_pin_7);

/*Sensor 1 Temperatura*/
temperatura_7 = my_dht_1.temperature;
Serial.println("E2S1T");
delay(5000);
Serial.println(temperatura_7);
delay(10000);
  
/*Sensor 1 Umidade*/
umidade_7 = my_dht_1.humidity;
Serial.println("E2S1U");
delay(5000);
Serial.println(umidade_7);
delay(10000);

/*Sensor 1 Solo*/
Serial.println("E2S1S");
delay(1000);
solo_A0 = analogRead(solo_pin_A0);
percentual_umidade = (solo_A0 * 100)/1023;
percentual_umidade = 100 - percentual_umidade;
Serial.println(percentual_umidade);
delay(1000);
}
