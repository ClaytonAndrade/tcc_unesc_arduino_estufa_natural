import requests
import json
#import urllib.request
import serial

#*********************************************************************************************
# Variaveis globais
#*********************************************************************************************
port = '/dev/ttyUSB1'

comunicacaoSerial = serial.Serial(port, 9600)

url = 'http://10.0.0.115:8080/'
headers={'Content-type':'application/json', 'Accept':'application/json'}

#*********************************************************************************************

#*********************************************************************************************
# Variaveis Estufa 2
#*********************************************************************************************
Estufa_2 = 2
identificacaoSensorEnum_1 = 2 #solo 3
identificacaoSensorEnum_2 = 8 #temperatura 3
identificacaoSensorEnum_3 = 9 #umidade 3


#*********************************************************************************************
# Metodos
#*********************************************************************************************
#persiste na base via post
def inserir(valorObtido, tipoLeitura, Estufa, identificacaoSensorEnum):
    endpoint= "leitura"
    data = {"valorObtido":valorObtido, 
            "tipoLeitura":tipoLeitura,
            "estufaModel":
                {"idEstufa":Estufa},
            "identificacaoSensorEnum":identificacaoSensorEnum
            }
    try:
        req = requests.post(url+endpoint, data=json.dumps(data),headers=headers)
        print (req.status_code)
        return req
    except:
        print("Servidor offline")

#*********************************************************************************************
# Execucao das requisicoes
#*********************************************************************************************

while (True):
    valorRecebido = comunicacaoSerial.readline()
    valorRecebido = valorRecebido[0:-2] 
    print(valorRecebido.decode('UTF-8'))
    
    if (valorRecebido.decode('UTF-8') == 'E2S1T'):
        valorRecebido = comunicacaoSerial.readline()
        valorRecebido = valorRecebido[0:-2]
        valorRecebido = valorRecebido.decode('UTF-8')
        print(valorRecebido, Estufa_2, identificacaoSensorEnum_2)
        inserir(valorRecebido, "Temperatura S1", Estufa_2, identificacaoSensorEnum_2)
        continue
     
    if (valorRecebido.decode('UTF-8') == 'E2S1U'):
        valorRecebido = comunicacaoSerial.readline()
        valorRecebido = valorRecebido[0:-2]
        valorRecebido = valorRecebido.decode('UTF-8')
        print(valorRecebido, Estufa_2, identificacaoSensorEnum_3)
        inserir(valorRecebido, "Umidade Ar S1", Estufa_2, identificacaoSensorEnum_3)
        continue
    
    if (valorRecebido.decode('UTF-8') == 'E2S1S'):
        valorRecebido = comunicacaoSerial.readline()
        valorRecebido = valorRecebido[0:-2]
        valorRecebido = valorRecebido.decode('UTF-8')
        print(valorRecebido, Estufa_2, identificacaoSensorEnum_1)
        status = inserir(valorRecebido, "umidade solo", Estufa_2, identificacaoSensorEnum_1)
        print (status.status_code)
        continue

